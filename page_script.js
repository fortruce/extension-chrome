// This document gets injected straight into the page (DOM).

// prevent repeating ourselves & causing a cascade of websocket overrides.
if (window.FIO_HAS_RUN === true)
{
    console.debug("Already injected websocket rebinding");
}
else
{
    window.FIO_HAS_RUN = true;

    let OrigWebSocket = window.WebSocket;
    let callWebSocket = OrigWebSocket.apply.bind(OrigWebSocket);
    let wsAddListener = OrigWebSocket.prototype.addEventListener;
    wsAddListener = wsAddListener.call.bind(wsAddListener);
    window.WebSocket = function WebSocket(url, protocols)
    {
        let ws;
        if (!(this instanceof WebSocket))
        {
            // Called without 'new' (browsers will throw an error).
            ws = callWebSocket(this, arguments);
        }
        else if (arguments.length === 1)
        {
            ws = new OrigWebSocket(url);
        }
        else if (arguments.length >= 2)
        {
            ws = new OrigWebSocket(url, protocols);
        }
        else
        { // No arguments (browsers will throw an error)
            ws = new OrigWebSocket();
        }
    
        wsAddListener(ws, 'message', function(event)
        {
            //console.debug("Websocket message occurred");
            window.postMessage(
                { 
                    message: "websocket_update",
                    payload: event.data,
                }
            );
        });
        return ws;
    }.bind();
    
    window.WebSocket.prototype = OrigWebSocket.prototype;
    window.WebSocket.prototype.constructor = window.WebSocket;
    
    /*
    // Override send, if needed.
    let wsSend = OrigWebSocket.prototype.send;
    wsSend = wsSend.apply.bind(wsSend);
    OrigWebSocket.prototype.send = function(data)
    {
        // console.log("Sent message");
        return wsSend(this, arguments);
    };
    */

    window.addEventListener("DOMContentLoaded", function() {
        console.log("FIO: DOMContentLoaded hit");
        checkExist = setInterval(function() 
        {
            // Find the React container & derive the username from the page.
            var reactContainerStr = Object.keys(document.getElementById('container')).filter(k => k.startsWith("__reactContainer"))[0];
            var model = document.getElementById('container')[reactContainerStr].stateNode.current.child.child.child.pendingProps.store.getState().toJS();
            if ( model !== undefined && model.user !== undefined && model.user.user !== undefined && model.user.user.data !== undefined && model.user.user.data.username !== undefined )
            {
                if(typeof browser === "undefined") {
                    var browser = chrome;
                }
                console.log("FIO: Found user. Sending autorequestregister.");
                clearInterval(checkExist);
                window.postMessage({
                    message: "prep_registration",
                    username: model.user.user.data.username,
                });
            }
        }, 1000);
    });
    
}
