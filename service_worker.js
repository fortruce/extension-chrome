// Chrome-MV3 service worker
importScripts(["common/default_event_payload.js"]);
importScripts(["common/uploader.js"]);

if(typeof browser === "undefined") {
    var browser = chrome;
}

browser.action.enable();

browser.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
    // for some reason, even though it's specified in hosts_permission,
    // mv3 still tries to run this on EVERY page.
    if (changeInfo.status === 'complete' && /^https?:\/\/apex.prosperousuniverse.com/.test(tab.url))
    {
        browser.scripting
            .executeScript({
                target: { tabId: tabId },
                files: ["./foreground.js"]
            })
            .then(() => {
                console.log("Injected the foreground script.");
            });
    }
});

browser.runtime.onMessage.addListener(async function(request, sender, sendResponse)
{
    if (request.message === "login")
    {
        console.debug("Listener: FIO login");
        await browser.action.enable();
        let manifest = browser.runtime.getManifest();
        await browser.action.setIcon({path: manifest.action.default_icon});
    }
    if (request.message === "logout")
    {
        console.debug("Listener: FIO logout");
        await browser.action.enable();
        let manifest = browser.runtime.getManifest();
        await browser.action.setIcon({path: manifest.action.dim_icon})
    }
    if (request.message === "websocket_update")
    {
        //console.debug("Listener: websocket_update");
        //console.debug(request);
        ProcessMessage({ data: request.payload });
        return false; // no async response
    }
    
    if (request.message === "prep_registration")
    {
        var registration = await EvalRegistration(request.username)
        if(registration.UserName)
        {
            browser.storage.local.set({
                "reg_username": registration.UserName, 
                "reg_guid": registration.RegistrationGuid
            }, function(){});
        }
        else
        {
            browser.storage.local.remove(['reg_guid']);
            browser.storage.local.set({
                "reg_username": request.username,
            }, function(){});
        }
    }
});


async function EvalRegistration(username)
{
    arData = {
        messageType: "", 
        payload: {
            username: username,
            admin: false
        }
    };
    var url = fnar_url + "/auth/autorequestregister";
    return await fetch(url, {
        method: "POST",
        mode: "cors",
        credentials: 'omit',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(arData)
    })
    .then((response) => {
        if(response.status === 0 || response.status === 200)
        {
            console.debug("Logged in, sending data;");
            return response.json();
        }
        else if (response.status === 204)
        {
            return {registration: "existing"};
        }
        else
        {
            console.log("Failed to login, cannot send data");
            return {registration: "failed", code: response.status, message: response.body};
        }
    })
    .catch((error) => {
        console.log(error);
        return {registration: "failed", code: 0, message: error};
    });
}