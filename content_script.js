// Inject page_script.js directly into the webpage.
if(typeof browser === "undefined") {
    var browser = chrome;
}

function AddScriptToDOM(script_name)
{
    var s = document.createElement('script');
    s.src = browser.runtime.getURL(script_name);
    s.onload = function() {
        this.remove();
    };
    (document.head || document.documentElement).appendChild(s);
}

AddScriptToDOM("page_script.js");

lastevent = undefined;
browser.runtime.onMessage.addListener(event => {
    // prevent repeat events, since we tend to get several logon failures at onces.
    // best way to compare simple objects; JSON.stringify.
    if (JSON.stringify(lastevent) === JSON.stringify(event)) {
        return;
    }
    lastevent = event;

    if (event.message === "logon_failure") {
        alert("FIO Logon Failure: " + event.details);
    }
});
